import React from "react";

import { Column, Row, Img, Text, List, Button, Input, Stack } from "components";

const FrameFiftyNinePage = () => {
  return (
    <>
      <Column className="flex flex-col font-roboto items-center justify-start mx-[auto] w-[100%]">
        <Column className="bg-gray_50 flex flex-col items-center justify-start w-[100%]">
          <header className="w-[100%]">
            <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center max-w-[1600px] ml-[auto] mr-[auto] outline outline-[1px] outline-bluegray_50 md:p-[18px] p-[30px] sm:pl-[15px] sm:pr-[15px] sm:px-[15px] sm:py-[14px] w-[100%]">
              <Row className="flex flex-row md:flex-wrap sm:flex-wrap md:ml-[18px] ml-[30px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[95%] common-row-list">
                <ul className="flex flex-row items-center justify-between">
                  <li className="w-[12%] sm:w-[100%] sm:my-[10px] sm:mx-[0] sm:px-[0] flex-row flex">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center">
                      <Img
                        src="images/img_menu.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="menu"
                      />
                      <a
                        href={"javascript:"}
                        className="cursor-pointer flex-grow sm:ml-[14px] md:ml-[18px] ml-[30px] text-bluegray_600"
                        as="h4"
                        variant="h4"
                        rel="noreferrer"
                      >
                        Constructor
                      </a>
                    </Row>
                  </li>
                  <li className="w-[37%] sm:w-[100%] sm:my-[10px] sm:mx-[0] sm:px-[0] flex-row flex">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-center">
                      <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[25%]">
                        <a
                          href={"javascript:"}
                          className="cursor-pointer flex-grow font-bold text-bluegray_500 hover:text-bluegray_600"
                          variant="body2"
                          rel="noreferrer"
                        >
                          Today’s Deals
                        </a>
                        <Img
                          src="images/img_arrowdown.svg"
                          className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                          alt="arrowdown"
                        />
                      </Row>
                      <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-center md:ml-[18px] ml-[30px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[22%]">
                        <a
                          href={"javascript:"}
                          className="cursor-pointer flex-grow font-bold text-bluegray_500 hover:text-bluegray_600"
                          variant="body2"
                          rel="noreferrer"
                        >
                          Best Sellers
                        </a>
                        <Img
                          src="images/img_arrowdown.svg"
                          className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] ml-[10px] sm:ml-[4px] md:ml-[6px] sm:w-[14px] md:w-[18px] w-[30px]"
                          alt="arrowdown One"
                        />
                      </Row>
                      <a
                        href={"javascript:"}
                        className="cursor-pointer font-bold sm:ml-[14px] md:ml-[18px] ml-[30px] text-bluegray_500 hover:text-bluegray_600 w-[auto]"
                        variant="body2"
                        rel="noreferrer"
                      >
                        Customer Service
                      </a>
                      <a
                        href={"javascript:"}
                        className="cursor-pointer font-bold sm:ml-[14px] md:ml-[18px] ml-[30px] text-bluegray_500 hover:text-bluegray_600 w-[auto]"
                        variant="body2"
                        rel="noreferrer"
                      >
                        New Releases
                      </a>
                    </Row>
                  </li>
                  <li className="w-[10%] sm:w-[100%] sm:my-[10px] sm:mx-[0] sm:px-[0] flex-row flex">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-center">
                      <Img
                        src="images/img_search.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="search"
                      />
                      <a
                        href={"javascript:"}
                        className="cursor-pointer flex-grow font-bold md:ml-[12px] ml-[20px] sm:ml-[9px] text-bluegray_200 hover:text-bluegray_600"
                        variant="body4"
                        rel="noreferrer"
                      >
                        Search Goods ...
                      </a>
                    </Row>
                  </li>
                  <li className="w-[15%] sm:w-[100%] sm:my-[10px] sm:mx-[0] sm:px-[0] flex-row flex">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between">
                      <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center sm:mx-[0] sm:px-[0] sm:w-[100%] w-[50%]">
                        <Img
                          src="images/img_user.svg"
                          className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                          alt="user"
                        />
                        <a
                          href={"javascript:"}
                          className="cursor-pointer flex-grow font-bold md:ml-[12px] ml-[20px] sm:ml-[9px] text-bluegray_500 hover:text-bluegray_600"
                          variant="body2"
                          rel="noreferrer"
                        >
                          Account
                        </a>
                      </Row>
                      <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[37%]">
                        <Img
                          src="images/img_cart.svg"
                          className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                          alt="cart"
                        />
                        <a
                          href={"javascript:"}
                          className="cursor-pointer flex-grow font-bold text-bluegray_500 hover:text-bluegray_600"
                          variant="body2"
                          rel="noreferrer"
                        >
                          Bag
                        </a>
                      </Row>
                    </Row>
                  </li>
                </ul>
              </Row>
            </Row>
          </header>
          <Column className="flex flex-col items-center justify-start sm:mt-[23px] md:mt-[30px] mt-[50px] w-[100%]">
            <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-start justify-between sm:px-[15px] md:px-[34px] px-[55px] w-[100%]">
              <Column className="flex flex-col items-center justify-start max-w-[250px] ml-[auto] mr-[auto] sm:mx-[0] sm:pl-[15px] sm:pr-[15px] sm:px-[0] w-[100%]">
                <List
                  className="bg-white_A700 md:gap-[12px] gap-[20px] sm:gap-[9px] grid min-h-[auto] md:p-[15px] sm:p-[15px] p-[25px] rounded-radius10 shadow-bs1 w-[100%]"
                  orientation="vertical"
                >
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-end sm:mx-[0] sm:px-[0] sm:w-[100%] w-[51%]">
                      <Img
                        src="images/img_camera.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="camera"
                      />
                      <Text
                        className="flex-grow font-bold sm:mb-[2px] md:mb-[3px] mb-[5px] md:ml-[12px] ml-[20px] sm:ml-[9px] sm:mt-[4px] md:mt-[5px] mt-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Speakers
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-end sm:mx-[0] sm:px-[0] sm:w-[100%] w-[54%]">
                      <Img
                        src="images/img_hardwarerouter.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="Hardwarerouter"
                      />
                      <Text
                        className="flex-grow font-bold sm:mb-[2px] md:mb-[3px] mb-[5px] md:ml-[12px] ml-[20px] sm:ml-[9px] sm:mt-[4px] md:mt-[5px] mt-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Equipment
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright One"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[48%]">
                      <Img
                        src="images/img_contrast.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="contrast"
                      />
                      <Text
                        className="flex-grow font-bold text-bluegray_500"
                        variant="body4"
                      >
                        Controls
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Two"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center sm:mx-[0] sm:px-[0] sm:w-[100%] w-[59%]">
                      <Img
                        src="images/img_trash.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="trash"
                      />
                      <Text
                        className="flex-grow font-bold sm:ml-[10px] md:ml-[13px] ml-[21px] text-bluegray_500"
                        variant="body4"
                      >
                        Accessories
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Three"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[42%]">
                      <Img
                        src="images/img_music.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="music"
                      />
                      <Text
                        className="flex-grow font-bold text-bluegray_500"
                        variant="body4"
                      >
                        Audio
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Four"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-end sm:mx-[0] sm:px-[0] sm:w-[100%] w-[56%]">
                      <Img
                        src="images/img_menu_30X30.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="menu One"
                      />
                      <Text
                        className="flex-grow font-bold sm:mb-[2px] md:mb-[3px] mb-[5px] md:ml-[12px] ml-[20px] sm:ml-[9px] sm:mt-[4px] md:mt-[5px] mt-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Appliances
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Five"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center sm:mx-[0] sm:px-[0] sm:w-[100%] w-[63%]">
                      <Img
                        src="images/img_computer.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="computer"
                      />
                      <Text
                        className="flex-grow font-bold md:ml-[12px] ml-[20px] sm:ml-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Television Set
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Six"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-end sm:mx-[0] sm:px-[0] sm:w-[100%] w-[61%]">
                      <Img
                        src="images/img_location.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="location"
                      />
                      <Text
                        className="flex-grow font-bold sm:mb-[2px] md:mb-[3px] mb-[5px] md:ml-[12px] ml-[20px] sm:ml-[9px] sm:mt-[4px] md:mt-[5px] mt-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Digital Watch
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Seven"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center sm:mx-[0] sm:px-[0] sm:w-[100%] w-[61%]">
                      <Img
                        src="images/img_volume.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="volume"
                      />
                      <Text
                        className="flex-grow font-bold md:ml-[12px] ml-[20px] sm:ml-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Video Games
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Eight"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[45%]">
                      <Img
                        src="images/img_mobile.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="mobile"
                      />
                      <Text
                        className="flex-grow font-bold text-bluegray_500"
                        variant="body4"
                      >
                        Tablets
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Nine"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center sm:mx-[0] sm:px-[0] sm:w-[100%] w-[59%]">
                      <Img
                        src="images/img_calculator.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="calculator"
                      />
                      <Text
                        className="flex-grow font-bold md:ml-[12px] ml-[20px] sm:ml-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Accessories
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Ten"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-end sm:mx-[0] sm:px-[0] sm:w-[100%] w-[56%]">
                      <Img
                        src="images/img_computer_30X30.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="computer One"
                      />
                      <Text
                        className="flex-grow font-bold sm:mb-[2px] md:mb-[3px] mb-[5px] md:ml-[12px] ml-[20px] sm:ml-[9px] sm:mt-[4px] md:mt-[5px] mt-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Computers
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Eleven"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-end justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[48%]">
                      <Img
                        src="images/img_computer_1.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="computer Two"
                      />
                      <Text
                        className="flex-grow font-bold sm:mb-[2px] md:mb-[3px] mb-[5px] sm:mt-[4px] md:mt-[5px] mt-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Laptops
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Twelve"
                    />
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between my-[0] sm:px-[0] w-[100%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-end sm:mx-[0] sm:px-[0] sm:w-[100%] w-[62%]">
                      <Img
                        src="images/img_mobile.svg"
                        className="flex-shrink-0 sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                        alt="mobile One"
                      />
                      <Text
                        className="flex-grow font-bold sm:mb-[2px] md:mb-[3px] mb-[5px] md:ml-[12px] ml-[20px] sm:ml-[9px] sm:mt-[4px] md:mt-[5px] mt-[9px] text-bluegray_500"
                        variant="body4"
                      >
                        Smartphones
                      </Text>
                    </Row>
                    <Img
                      src="images/img_arrowright.svg"
                      className="sm:h-[15px] md:h-[19px] h-[30px] max-w-[100%] sm:w-[14px] md:w-[18px] w-[30px]"
                      alt="arrowright Thirteen"
                    />
                  </Row>
                </List>
                <Column className="flex flex-col items-center justify-start sm:mt-[14px] md:mt-[18px] mt-[30px] outline outline-[1px] outline-bluegray_50 md:p-[24px] p-[40px] sm:px-[15px] sm:py-[19px] rounded-radius10 w-[100%]">
                  <Text
                    className="leading-[30.00px] md:leading-[normal] sm:leading-[normal] sm:mx-[0] text-bluegray_500 text-center sm:w-[100%] w-[76%]"
                    as="h6"
                    variant="h6"
                  >
                    The New Smart Watches
                  </Text>
                  <Img
                    src="images/img_image.png"
                    className="max-w-[100%] sm:mt-[19px] md:mt-[24px] mt-[40px] sm:w-[100%] w-[84%]"
                    alt="Image"
                  />
                  <Button
                    className="cursor-pointer font-black mb-[15px] sm:mb-[7px] md:mb-[9px] min-w-[58%] sm:mt-[19px] md:mt-[24px] mt-[40px] text-[12px] text-center text-white_A700 w-[max-content]"
                    shape="CircleBorder20"
                    size="md"
                    variant="OutlineDeeporangeA2004c"
                  >
                    View More
                  </Button>
                </Column>
                <Column className="flex flex-col items-center justify-end sm:mt-[14px] md:mt-[18px] mt-[30px] outline outline-[1px] outline-bluegray_50 md:p-[28px] p-[46px] sm:px-[15px] sm:py-[22px] rounded-radius10 w-[100%]">
                  <Img
                    src="images/img_image_133X140.png"
                    className="max-w-[100%] sm:mt-[11px] md:mt-[14px] mt-[23px] sm:w-[100%] w-[89%]"
                    alt="Image One"
                  />
                  <Text
                    className="leading-[30.00px] md:leading-[normal] sm:leading-[normal] sm:mt-[19px] md:mt-[24px] mt-[40px] sm:mx-[0] text-bluegray_500 text-center sm:w-[100%] w-[82%]"
                    as="h6"
                    variant="h6"
                  >
                    New Tablets
                    <br />
                    Samsung
                  </Text>
                </Column>
              </Column>
              <Column className="flex flex-col items-center justify-end max-w-[1110px] ml-[auto] md:mr-[21px] mr-[auto] sm:mx-[0] sm:pl-[15px] sm:pr-[15px] sm:pt-[16px] md:pt-[21px] pt-[34px] sm:px-[0] w-[100%]">
                <Column className="flex flex-col items-center justify-start md:pt-[12px] pt-[20px] sm:pt-[9px] w-[100%]">
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-start justify-between w-[100%]">
                    <Column className="flex flex-col justify-start mt-[11px] sm:mt-[5px] md:mt-[6px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[31%]">
                      <Text
                        className="text-bluegray_600 w-[auto]"
                        as="h2"
                        variant="h2"
                      >
                        Hypermarket Electronics
                      </Text>
                      <Text
                        className="mt-[1px] text-indigo_900_5d uppercase w-[auto]"
                        as="h1"
                        variant="h1"
                      >
                        NEW
                      </Text>
                    </Column>
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[35%]">
                      <Input
                        className="font-bold p-[0] text-[12px] placeholder:text-bluegray_400 text-bluegray_400 w-[100%]"
                        wrapClassName="flex sm:mx-[0] sm:w-[100%] w-[64%]"
                        name="Input"
                        placeholder="Your Mail"
                        prefix={
                          <Img
                            src="images/img_mail.svg"
                            className="ml-[2px] mr-[10px] sm:mr-[4px] md:mr-[6px] my-[auto]"
                            alt="mail"
                          />
                        }
                      ></Input>
                      <Button
                        className="cursor-pointer font-black min-w-[31%] text-[12px] text-center text-white_A700 w-[max-content]"
                        shape="CircleBorder25"
                        size="lg"
                        variant="OutlineDeeporangeA20019"
                      >
                        Subscribe
                      </Button>
                    </Row>
                  </Row>
                </Column>
                <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mt-[31px] md:mt-[40px] mt-[65px] w-[100%]">
                  <Stack className="bg-gray_100 h-[350px] sm:pr-[15px] md:pr-[24px] pr-[40px] relative rounded-radius10 shadow-bs1 sm:w-[100%] w-[49%]">
                    <Stack className="absolute h-[350px] sm:w-[100%] w-[72%]">
                      <div className="absolute bg-bluegray_50 sm:h-[168px] md:h-[217px] h-[350px] rounded-radius1935 w-[100%]"></div>
                      <div className="absolute bg-indigo_50 bottom-[0] md:h-[118px] h-[190px] sm:h-[92px] left-[0] rounded-radius1685 sm:w-[100%] w-[88%]"></div>
                    </Stack>
                    <Row className="absolute flex flex-row md:flex-wrap sm:flex-wrap inset-x-[0] items-center justify-between sm:mx-[0] mx-[auto] sm:px-[0] top-[11%] sm:w-[100%] w-[86%]">
                      <Button
                        className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]"
                        variant="icbOutlineBluegray90007"
                      >
                        <Img
                          src="images/img_menu.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="menu Two"
                        />
                      </Button>
                      <Button
                        className="cursor-pointer font-black min-w-[12%] my-[10px] sm:my-[4px] md:my-[6px] text-[8px] text-center text-white_A700 w-[max-content]"
                        shape="CircleBorder10"
                        size="sm"
                        variant="FillDeeporangeA200"
                      >
                        New iPad
                      </Button>
                      <Button
                        className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]"
                        variant="icbOutlineBluegray90007"
                      >
                        <Img
                          src="images/img_reply.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="reply"
                        />
                      </Button>
                    </Row>
                    <Column className="absolute bottom-[14%] flex flex-col inset-x-[0] items-center justify-start sm:mx-[0] mx-[auto] sm:px-[0] sm:w-[100%] w-[80%]">
                      <Text
                        className="text-bluegray_600 w-[auto]"
                        as="h3"
                        variant="h3"
                      >
                        The New iPad
                      </Text>
                      <Img
                        src="images/img_image_129X430.png"
                        className="max-w-[100%] sm:mt-[14px] md:mt-[18px] mt-[30px] w-[100%]"
                        alt="Image Four"
                      />
                    </Column>
                  </Stack>
                  <Column className="bg-black_900 flex flex-col items-center sm:mx-[0] sm:p-[15px] md:p-[24px] p-[40px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[49%]">
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:px-[0] w-[100%]">
                      <Button className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]">
                        <Img
                          src="images/img_menu_40X40.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="menu Three"
                        />
                      </Button>
                      <Button className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]">
                        <Img
                          src="images/img_reply_40X40.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="reply One"
                        />
                      </Button>
                    </Row>
                    <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-center sm:mb-[2px] md:mb-[3px] mb-[5px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[83%]">
                      <Img
                        src="images/img_image_197X172.png"
                        className="max-w-[100%] sm:w-[100%] w-[46%]"
                        alt="Image Five"
                      />
                      <Column className="flex flex-col items-center md:ml-[29px] ml-[47px] sm:mx-[0] pb-[4px] sm:px-[0] sm:w-[100%] w-[43%]">
                        <Button
                          className="cursor-pointer font-black min-w-[24%] text-[8px] text-center text-white_A700 w-[max-content]"
                          shape="CircleBorder10"
                          size="sm"
                          variant="FillDeeporangeA200"
                        >
                          NEW
                        </Button>
                        <Text
                          className="sm:mt-[14px] md:mt-[18px] mt-[30px] text-white_A700 w-[auto]"
                          as="h3"
                          variant="h3"
                        >
                          AirPods Pro
                        </Text>
                        <Text
                          className="font-medium leading-[30.00px] md:leading-[normal] sm:leading-[normal] sm:mt-[14px] md:mt-[18px] mt-[30px] text-center text-white_A700 w-[100%]"
                          variant="body2"
                        >
                          Magic like you’ve <br />
                          never heard.
                        </Text>
                        <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-start justify-between sm:mt-[16px] md:mt-[21px] mt-[35px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[70%]">
                          <Text
                            className="text-deep_orange_A200 w-[auto]"
                            variant="body3"
                          >
                            Learn More
                          </Text>
                          <Text
                            className="mt-[1px] text-deep_orange_A200 w-[auto]"
                            variant="body3"
                          >
                            Buy
                          </Text>
                        </Row>
                      </Column>
                    </Row>
                  </Column>
                </Row>
                <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mt-[14px] md:mt-[18px] mt-[30px] w-[100%]">
                  <Stack className="bg-black_900 h-[350px] sm:p-[15px] md:p-[18px] p-[30px] relative rounded-radius10 shadow-bs1 sm:w-[167px] md:w-[216px] w-[350px]">
                    <Row className="absolute flex flex-row md:flex-wrap sm:flex-wrap inset-x-[0] items-center justify-between sm:mx-[0] mx-[auto] sm:px-[0] top-[9%] sm:w-[100%] w-[83%]">
                      <Button className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]">
                        <Img
                          src="images/img_menu_40X40.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="menu Four"
                        />
                      </Button>
                      <Button className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]">
                        <Img
                          src="images/img_reply_40X40.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="reply Two"
                        />
                      </Button>
                    </Row>
                    <Column className="absolute flex flex-col h-[max-content] inset-[0] items-center justify-center m-[auto] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[44%]">
                      <Img
                        src="images/img_signal.svg"
                        className="max-w-[100%] sm:w-[100%] w-[61%]"
                        alt="signal"
                      />
                      <Img
                        src="images/img_image_120X120.png"
                        className="h-[120px] sm:h-[58px] md:h-[75px] max-w-[100%] sm:mt-[14px] md:mt-[18px] mt-[30px] w-[120px] sm:w-[57px] md:w-[74px]"
                        alt="Image Six"
                      />
                      <Text
                        className="sm:mt-[18px] md:mt-[23px] mt-[38px] text-white_A700 w-[auto]"
                        variant="body1"
                      >
                        Compare Thermostats
                      </Text>
                    </Column>
                  </Stack>
                  <Stack className="bg-indigo_50 h-[350px] sm:pt-[15px] md:pt-[18px] pt-[30px] relative rounded-radius10 shadow-bs1 sm:w-[167px] md:w-[216px] w-[350px]">
                    <Column className="absolute flex flex-col items-center justify-start w-[100%]">
                      <Column className="flex flex-col items-center justify-start w-[100%]">
                        <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between sm:mx-[0] sm:px-[0] sm:w-[100%] w-[83%]">
                          <Button
                            className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]"
                            variant="icbFillWhiteA700"
                          >
                            <Img
                              src="images/img_menu.svg"
                              className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                              alt="menu Five"
                            />
                          </Button>
                          <Button
                            className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]"
                            variant="icbFillWhiteA700"
                          >
                            <Img
                              src="images/img_reply.svg"
                              className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                              alt="reply Three"
                            />
                          </Button>
                        </Row>
                        <div className="bg-gradient  md:h-[112px] h-[180px] sm:h-[87px] mt-[100px] sm:mt-[47px] md:mt-[61px] rounded-radius175 w-[100%]"></div>
                      </Column>
                    </Column>
                    <Column className="absolute flex flex-col h-[max-content] inset-[0] items-center justify-center m-[auto] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[50%]">
                      <Img
                        src="images/img_logo.png"
                        className="max-w-[100%] w-[35%]"
                        alt="Logo"
                      />
                      <Img
                        src="images/img_image_93X175.png"
                        className="max-w-[100%] sm:mt-[19px] md:mt-[24px] mt-[40px] w-[100%]"
                        alt="Image Seven"
                      />
                      <Text
                        className="sm:mt-[23px] md:mt-[29px] mt-[48px] text-white_A700 w-[auto]"
                        variant="body1"
                      >
                        Google Stadia
                      </Text>
                    </Column>
                  </Stack>
                  <Stack className="bg-black_900 h-[350px] sm:p-[15px] md:p-[18px] p-[30px] relative rounded-radius10 shadow-bs1 sm:w-[167px] md:w-[216px] w-[350px]">
                    <Row className="absolute flex flex-row md:flex-wrap sm:flex-wrap inset-x-[0] items-center justify-between sm:mx-[0] mx-[auto] sm:px-[0] top-[9%] sm:w-[100%] w-[83%]">
                      <Button className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]">
                        <Img
                          src="images/img_menu_40X40.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="menu Six"
                        />
                      </Button>
                      <Button className="flex sm:h-[20px] md:h-[25px] h-[40px] items-center justify-center rounded-radius50 sm:w-[19px] md:w-[24px] w-[40px]">
                        <Img
                          src="images/img_reply_40X40.svg"
                          className="h-[28px] sm:h-[14px] md:h-[18px] flex items-center justify-center"
                          alt="reply Four"
                        />
                      </Button>
                    </Row>
                    <Column className="absolute bottom-[10%] flex flex-col inset-x-[0] items-center justify-start sm:mx-[0] mx-[auto] sm:px-[0] sm:w-[100%] w-[69%]">
                      <Stack className="h-[190px] relative w-[100%]">
                        <Img
                          src="images/img_samsung.svg"
                          className="absolute bottom-[38%] h-[35px] max-w-[100%] w-[100%]"
                          alt="Samsung"
                        />
                        <Img
                          src="images/img_image_190X110.png"
                          className="absolute h-[190px] inset-x-[27%] max-w-[100%] sm:w-[100%] w-[47%]"
                          alt="Image Eight"
                        />
                      </Stack>
                      <Text
                        className="sm:mt-[23px] md:mt-[29px] mt-[48px] text-white_A700 w-[auto]"
                        variant="body1"
                      >
                        Galaxy Watch
                      </Text>
                    </Column>
                  </Stack>
                </Row>
                <Stack className="h-[220px] sm:mt-[14px] md:mt-[18px] mt-[30px] md:p-[39px] p-[64px] sm:px-[15px] sm:py-[30px] relative w-[100%]">
                  <Text
                    className="absolute bottom-[9%] inset-x-[0] mx-[auto] text-bluegray_600 w-[max-content]"
                    as="h2"
                    variant="h2"
                  >
                    Hypermarket Electronics
                  </Text>
                  <Text
                    className="absolute h-[max-content] inset-[0] justify-center m-[auto] text-indigo_900_5d uppercase w-[max-content]"
                    as="h1"
                    variant="h1"
                  >
                    NEW
                  </Text>
                </Stack>
                <List
                  className="sm:gap-[14px] md:gap-[18px] gap-[30px] grid min-h-[auto] sm:mt-[14px] md:mt-[18px] mt-[30px] w-[100%]"
                  orientation="vertical"
                >
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between w-[100%]">
                    <Column className="bg-black_900 flex flex-col items-center sm:mx-[0] sm:pb-[15px] md:pb-[27px] pb-[44px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[23%]">
                      <Stack className="h-[160px] relative w-[100%]">
                        <div className="absolute bg-amber_400 h-[120px] sm:h-[58px] md:h-[75px] rounded-radius1275 top-[0] w-[100%]"></div>
                        <Img
                          src="images/img_image_91X130.png"
                          className="absolute bottom-[0] h-[91px] inset-x-[0] max-w-[100%] mx-[auto] sm:w-[100%] w-[51%]"
                          alt="Image Nine"
                        />
                      </Stack>
                      <Column className="flex flex-col items-center justify-start sm:mt-[25px] md:mt-[33px] mt-[54px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[54%]">
                        <Text
                          className="text-amber_400 w-[auto]"
                          variant="body5"
                        >
                          BLACK FRIDAY
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[29px] text-white_A700 w-[auto]"
                          variant="body2"
                        >
                          Gamepad Xbox One X
                        </Text>
                        <Text
                          className="font-black sm:mt-[11px] md:mt-[15px] mt-[25px] text-white_A700 w-[auto]"
                          variant="body4"
                        >
                          $950.00
                        </Text>
                      </Column>
                    </Column>
                    <Column className="bg-black_900 flex flex-col items-center sm:mx-[0] sm:pb-[15px] md:pb-[24px] pb-[40px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[23%]">
                      <Stack className="h-[160px] relative w-[100%]">
                        <Img
                          src="images/img_image_138X255.png"
                          className="absolute h-[138px] max-w-[100%] top-[0] w-[100%]"
                          alt="Image One"
                        />
                        <Img
                          src="images/img_image_100X130.png"
                          className="absolute bottom-[0] h-[100px] inset-x-[0] max-w-[100%] mx-[auto] sm:w-[100%] w-[51%]"
                          alt="Image Two"
                        />
                      </Stack>
                      <Column className="flex flex-col items-center justify-start sm:mt-[23px] md:mt-[30px] mt-[50px] sm:mx-[0] pr-[1px] sm:px-[0] py-[1px] sm:w-[100%] w-[29%]">
                        <Text
                          className="mt-[2px] text-white_A700_7f w-[auto]"
                          variant="body5"
                        >
                          TABLETS
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[28px] text-white_A700 w-[auto]"
                          variant="body2"
                        >
                          iPad Pro 11
                        </Text>
                        <Text
                          className="font-black mb-[2px] sm:mt-[12px] md:mt-[16px] mt-[26px] text-white_A700 w-[auto]"
                          variant="body4"
                        >
                          $450.00
                        </Text>
                      </Column>
                    </Column>
                    <Stack className="bg-black_900 h-[350px] sm:pb-[15px] md:pb-[27px] pb-[44px] relative rounded-radius10 shadow-bs1 w-[23%]">
                      <Stack
                        className="absolute bg-cover bg-no-repeat h-[270px] sm:p-[15px] md:p-[32px] p-[52px] w-[100%]"
                        style={{
                          backgroundImage: "url('images/img_group2.png')",
                        }}
                      >
                        <Img
                          src="images/img_image_107X130.png"
                          className="absolute h-[107px] inset-x-[0] max-w-[100%] mx-[auto] top-[19%] sm:w-[100%] w-[51%]"
                          alt="Image Three"
                        />
                      </Stack>
                      <Column className="absolute bottom-[13%] flex flex-col inset-x-[0] items-center justify-start sm:mx-[0] mx-[auto] sm:px-[0] sm:w-[100%] w-[40%]">
                        <Text
                          className="text-white_A700_7f w-[auto]"
                          variant="body5"
                        >
                          COMPUTERS
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[29px] text-white_A700 w-[auto]"
                          variant="body2"
                        >
                          Pro Display XDR
                        </Text>
                        <Text
                          className="font-black sm:mt-[11px] md:mt-[15px] mt-[25px] text-white_A700 w-[auto]"
                          variant="body4"
                        >
                          $950.00
                        </Text>
                      </Column>
                    </Stack>
                    <Column className="bg-black_900 flex flex-col items-center sm:mx-[0] sm:p-[15px] md:p-[24px] p-[40px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[23%]">
                      <Img
                        src="images/img_image_120X49.png"
                        className="max-w-[100%] w-[29%]"
                        alt="Image Four"
                      />
                      <Column className="flex flex-col items-center justify-start mb-[4px] sm:mt-[25px] md:mt-[33px] mt-[54px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[59%]">
                        <Text
                          className="text-white_A700_7f w-[auto]"
                          variant="body5"
                        >
                          COMPUTERS
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[29px] text-white_A700 w-[auto]"
                          variant="body2"
                        >
                          Pro Display XDR
                        </Text>
                        <Text
                          className="font-black sm:mt-[11px] md:mt-[15px] mt-[25px] text-white_A700 w-[auto]"
                          variant="body4"
                        >
                          $950.00
                        </Text>
                      </Column>
                    </Column>
                  </Row>
                  <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-center justify-between w-[100%]">
                    <Column className="bg-white_A700 flex flex-col items-center sm:mx-[0] sm:p-[15px] md:p-[24px] p-[40px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[23%]">
                      <Img
                        src="images/img_image_120X94.png"
                        className="max-w-[100%] sm:w-[100%] w-[54%]"
                        alt="Image Ten"
                      />
                      <Column className="flex flex-col items-center justify-start mb-[4px] sm:mt-[25px] md:mt-[33px] mt-[54px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[76%]">
                        <Text
                          className="text-bluegray_400 w-[auto]"
                          variant="body5"
                        >
                          PHONES
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[29px] text-bluegray_600 w-[auto]"
                          variant="body2"
                        >
                          Samsung Galaxy s10
                        </Text>
                        <Text
                          className="font-black sm:mt-[11px] md:mt-[15px] mt-[25px] text-bluegray_500 w-[auto]"
                          variant="body4"
                        >
                          $950.00
                        </Text>
                      </Column>
                    </Column>
                    <Column className="bg-white_A700 flex flex-col items-center sm:mx-[0] sm:p-[15px] md:p-[27px] p-[44px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[23%]">
                      <Img
                        src="images/img_image_95X150.png"
                        className="max-w-[100%] md:mt-[11px] mt-[19px] sm:mt-[9px] sm:w-[100%] w-[90%]"
                        alt="Image One One"
                      />
                      <Column className="flex flex-col items-center justify-start sm:mt-[25px] md:mt-[33px] mt-[54px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[80%]">
                        <Text
                          className="text-bluegray_400 w-[auto]"
                          variant="body5"
                        >
                          COMPUTERS
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[29px] text-bluegray_600 w-[auto]"
                          variant="body2"
                        >
                          Samsung Galaxy s10
                        </Text>
                        <Text
                          className="font-black sm:mt-[11px] md:mt-[15px] mt-[25px] text-bluegray_500 w-[auto]"
                          variant="body4"
                        >
                          $950.00
                        </Text>
                      </Column>
                    </Column>
                    <Column className="bg-white_A700 flex flex-col items-center sm:mx-[0] sm:p-[15px] md:p-[24px] p-[40px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[23%]">
                      <Img
                        src="images/img_image_120X98.png"
                        className="max-w-[100%] sm:w-[100%] w-[57%]"
                        alt="Image Two One"
                      />
                      <Column className="flex flex-col items-center justify-start mb-[4px] sm:mt-[25px] md:mt-[33px] mt-[54px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[63%]">
                        <Text
                          className="text-bluegray_400 w-[auto]"
                          variant="body5"
                        >
                          COMPUTERS
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[29px] text-bluegray_600 w-[auto]"
                          variant="body2"
                        >
                          Portable Speaker
                        </Text>
                        <Text
                          className="font-black sm:mt-[11px] md:mt-[15px] mt-[25px] text-bluegray_500 w-[auto]"
                          variant="body4"
                        >
                          $950.00
                        </Text>
                      </Column>
                    </Column>
                    <Column className="bg-white_A700 flex flex-col items-center sm:mx-[0] sm:p-[15px] md:p-[21px] p-[35px] rounded-radius10 shadow-bs1 sm:w-[100%] w-[23%]">
                      <Img
                        src="images/img_image_124X110.png"
                        className="max-w-[100%] sm:w-[100%] w-[60%]"
                        alt="Image Three One"
                      />
                      <Column className="flex flex-col items-center justify-start sm:mb-[3px] md:mb-[4px] mb-[8px] sm:mt-[25px] md:mt-[33px] mt-[54px] sm:mx-[0] sm:px-[0] sm:w-[100%] w-[86%]">
                        <Text
                          className="text-bluegray_400 w-[auto]"
                          variant="body5"
                        >
                          TABLETS
                        </Text>
                        <Text
                          className="font-bold sm:mt-[13px] md:mt-[17px] mt-[28px] text-bluegray_600 w-[auto]"
                          variant="body2"
                        >
                          Microsoft Surface Studio
                        </Text>
                        <Text
                          className="font-black sm:mt-[12px] md:mt-[16px] mt-[26px] text-bluegray_500 w-[auto]"
                          variant="body4"
                        >
                          $950.00
                        </Text>
                      </Column>
                    </Column>
                  </Row>
                </List>
              </Column>
            </Row>
            <Column className="border-2 border-bluegray_50 border-solid flex flex-col items-center justify-start sm:mt-[14px] md:mt-[18px] mt-[30px] sm:pb-[28px] md:pb-[37px] pb-[60px] w-[100%]">
              <Column className="flex flex-col items-center justify-start w-[100%]">
                <Row className="flex flex-row md:flex-wrap sm:flex-wrap items-start justify-between sm:p-[2px] md:p-[3px] p-[6px] w-[100%]">
                  <Column className="flex flex-col justify-start max-w-[164px] md:ml-[45px] ml-[auto] mr-[auto] sm:mt-[28px] md:mt-[36px] mt-[59px] sm:mx-[0] sm:pl-[15px] sm:pr-[15px] sm:px-[0] w-[100%]">
                    <Text
                      className="text-bluegray_600 w-[auto]"
                      as="h5"
                      variant="h5"
                    >
                      Constructor
                    </Text>
                    <Text
                      className="font-bold leading-[30.00px] md:leading-[normal] sm:leading-[normal] sm:mt-[22px] md:mt-[28px] mt-[46px] text-bluegray_200 w-[100%]"
                      variant="body2"
                    >
                      12 Water St. Vacouver, BC
                      <br />
                      V6B 132 United States
                    </Text>
                  </Column>
                  <List
                    className="gap-[159.5px] sm:gap-[76px] md:gap-[98px] grid sm:grid-cols-1 md:grid-cols-2 grid-cols-3 max-w-[760px] min-h-[auto] ml-[auto] mr-[auto] sm:mt-[29px] md:mt-[38px] mt-[62px] sm:pl-[15px] sm:pr-[15px] w-[100%]"
                    orientation="horizontal"
                  >
                    <Column className="flex flex-col justify-start sm:px-[0] w-[100%]">
                      <Text
                        className="font-medium text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Corporate sales
                      </Text>
                      <Text
                        className="font-medium sm:mt-[11px] md:mt-[14px] mt-[24px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Feedback
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[26px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Jobs
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[26px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        News
                      </Text>
                      <Text
                        className="font-medium sm:mt-[11px] md:mt-[15px] mt-[25px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Sales Rules
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[27px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        For partners
                      </Text>
                    </Column>
                    <Column className="flex flex-col justify-start sm:px-[0] w-[100%]">
                      <Text
                        className="font-medium text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Bonus program
                      </Text>
                      <Text
                        className="font-medium sm:mt-[11px] md:mt-[14px] mt-[24px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Gift Сards
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[27px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Bill Payment Verification
                      </Text>
                      <Text
                        className="font-medium sm:mt-[11px] md:mt-[14px] mt-[24px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Loans
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[27px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Delivery
                      </Text>
                      <Text
                        className="font-medium sm:mt-[11px] md:mt-[14px] mt-[24px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Service centers
                      </Text>
                    </Column>
                    <Column className="flex flex-col justify-start sm:px-[0] w-[100%]">
                      <Text
                        className="font-medium text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        How to place an order
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[26px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Ways of payment
                      </Text>
                      <Text
                        className="font-medium sm:mt-[11px] md:mt-[15px] mt-[25px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Exchange and return of goods
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[26px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Warranty service
                      </Text>
                      <Text
                        className="font-medium sm:mt-[11px] md:mt-[14px] mt-[24px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Order status
                      </Text>
                      <Text
                        className="font-medium sm:mt-[12px] md:mt-[16px] mt-[27px] text-bluegray_500 w-[auto]"
                        variant="body2"
                      >
                        Knowledge base
                      </Text>
                    </Column>
                  </List>
                  <Column className="flex flex-col justify-start max-w-[197px] ml-[auto] md:mr-[45px] mr-[auto] sm:mt-[25px] md:mt-[32px] mt-[53px] sm:mx-[0] sm:pl-[15px] sm:pr-[15px] pr-[1px] pt-[1px] sm:px-[0] w-[100%]">
                    <Text
                      className="font-bold mt-[3px] text-bluegray_200 w-[auto]"
                      variant="body4"
                    >
                      2030 Comapny. All Rights Reserved.
                    </Text>
                    <Text
                      className="font-bold sm:mt-[11px] md:mt-[14px] mt-[23px] text-bluegray_200 w-[auto]"
                      variant="body4"
                    >
                      Terms & Conditions
                    </Text>
                    <Text
                      className="font-bold sm:mt-[13px] md:mt-[17px] mt-[29px] text-bluegray_200 w-[auto]"
                      variant="body4"
                    >
                      Privacy Policy
                    </Text>
                  </Column>
                </Row>
              </Column>
            </Column>
          </Column>
        </Column>
      </Column>
    </>
  );
};

export default FrameFiftyNinePage;
