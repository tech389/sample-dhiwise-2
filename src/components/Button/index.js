import React from "react";
import PropTypes from "prop-types";

const shapes = {
  CircleBorder10: "rounded-radius10",
  CircleBorder20: "rounded-radius20",
  CircleBorder25: "rounded-radius25",
  icbCircleBorder20: "rounded-radius20",
};
const variants = {
  FillDeeporangeA200: "bg-deep_orange_A200 text-white_A700",
  OutlineDeeporangeA2004c: "bg-deep_orange_A200 shadow-bs text-white_A700",
  OutlineDeeporangeA20019: "bg-deep_orange_A200 shadow-bs2 text-white_A700",
  icbFillWhiteA70019: "bg-white_A700_19",
  icbOutlineBluegray90007: "bg-white_A700 shadow-bs1",
  icbFillWhiteA700: "bg-white_A700",
};
const sizes = {
  sm: "sm:p-[2px] md:p-[3px] p-[6px]",
  md: "p-[14px] sm:p-[6px] md:p-[8px]",
  lg: "md:p-[11px] p-[19px] sm:px-[15px] sm:py-[9px]",
  smIcn: "sm:p-[2px] md:p-[3px] p-[6px]",
};

const Button = ({
  children,
  className = "",
  leftIcon,
  rightIcon,
  shape,
  variant,
  size,
  ...restProps
}) => {
  return (
    <button
      className={`${className} ${shapes[shape] || ""} ${
        variants[variant] || ""
      } ${sizes[size] || ""} `}
      {...restProps}
    >
      {!!leftIcon && leftIcon}
      {children}
      {!!rightIcon && rightIcon}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  shape: PropTypes.oneOf([
    "CircleBorder10",
    "CircleBorder20",
    "CircleBorder25",
    "icbCircleBorder20",
  ]),
  variant: PropTypes.oneOf([
    "FillDeeporangeA200",
    "OutlineDeeporangeA2004c",
    "OutlineDeeporangeA20019",
    "icbFillWhiteA70019",
    "icbOutlineBluegray90007",
    "icbFillWhiteA700",
  ]),
  size: PropTypes.oneOf(["sm", "md", "lg", "smIcn"]),
};
Button.defaultProps = {
  className: "",
  shape: "icbCircleBorder20",
  variant: "icbFillWhiteA70019",
  size: "smIcn",
};

export { Button };
