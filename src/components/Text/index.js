import React from "react";
const variantClasses = {
  h1: "font-black md:text-[48px] sm:text-[48px] text-[90px]",
  h2: "font-bold sm:text-[26px] md:text-[28px] text-[30px]",
  h3: "font-bold sm:text-[20px] md:text-[22px] text-[24px]",
  h4: "font-bold text-[20px]",
  h5: "font-bold text-[18px]",
  h6: "font-bold text-[16px]",
  body1: "font-bold text-[15px]",
  body2: "text-[14px]",
  body3: "font-bold text-[13px]",
  body4: "text-[12px]",
  body5: "font-black text-[10px]",
};
const Text = ({ children, className, variant, as, ...restProps }) => {
  const Component = as || "span";
  return (
    <Component
      className={`${className} ${variantClasses[variant]}`}
      {...restProps}
    >
      {children}
    </Component>
  );
};

export { Text };
